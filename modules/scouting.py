from operator import *

def calcScore(team):
    team[-1]['score'] += (int(team[-1]["mineralsInHigh"])*5)
    team[-1]['score'] += (int(team[-1]["mineralsInSquare"])*2)
    team[-1]['score'] += team[-1]['land']*30
    team[-1]['score'] += team[-1]['touch']*10
    team[-1]['score'] += team[-1]['mineral']*25
    team[-1]['score'] += team[-1]['kamea']*15
    team[-1]['score'] += team[-1]['on']*15
    team[-1]['score'] += team[-1]['in']*25
    team[-1]['score'] += team[-1]['hang']*50

def calcPoints(teamDict, number, land, touch, kamea, mineral, mineralsInSquare, mineralsInHigh, endGame):
    if not (number in teamDict):
        teamDict[number] = list()
    teamDict[number].append({"mineralsInSquare": mineralsInSquare, "mineralsInHigh": mineralsInHigh})
    if land:
        teamDict[number][-1]['land'] = 1
    else:
        teamDict[number][-1]['land'] = 0
    if touch:
        teamDict[number][-1]["touch"] = 1
    else:
        teamDict[number][-1]["touch"] = 0
    if mineral:
        teamDict[number][-1]["mineral"] = 1
    else:
        teamDict[number][-1]["mineral"] = 0
    if kamea:
        teamDict[number][-1]["kamea"] = 1
    else:
        teamDict[number][-1]["kamea"] = 0

    if endGame == "on":
        teamDict[number][-1]["on"] = 1
        teamDict[number][-1]["in"] = 0
        teamDict[number][-1]["hang"] = 0
    elif endGame == "in":
        teamDict[number][-1]["in"] = 1
        teamDict[number][-1]["on"] = 0
        teamDict[number][-1]["hang"] = 0
    elif endGame == "hang":
        teamDict[number][-1]["in"] = 0
        teamDict[number][-1]["on"] = 0
        teamDict[number][-1]["hang"] = 1
    else:
        teamDict[number][-1]["in"] = 0
        teamDict[number][-1]["on"] = 0
        teamDict[number][-1]["hang"] = 0
    teamDict[number][-1]["score"] = 0
    calcScore(teamDict[number])
    return sort(teamDict)

def calcTotalScore(teamDict, key):
    score = 0
    for num in teamDict[key]:
        score += num["score"]
    return score

def sort(teamDict):
    l = []
    s = []
    for key in teamDict:
        l.append(key)
        s.append(calcTotalScore(teamDict, key))

    n = len(s)
    # Traverse through all array elements
    for i in range(n):
        # Last i elements are already in place
        for j in range(0, n-i-1):
            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if s[j] < s[j+1] :
                s[j], s[j+1] = s[j+1], s[j]
                l[j], l[j+1] = l[j+1], l[j]
    return organizedData(teamDict, l)

def organizedData(teamDict, l):
    data = ''
    for team in l:
        data += '''
        <tr onclick="window.location.href='table.html?team={}';">
            <th style="width: 50%"> {} </th><th style="width: 50%">{}</th>
        </tr>
        '''.format(team, team, calcTotalScore(teamDict, team))
    return data

def organizedTeamData(teamDict, team):
    data = ''
    for num in teamDict[team]:
        data += '''
        <tr>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
                
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
        
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 5%">{}</th>
        </tr>
        '''.format(num["land"], num["mineral"], num["kamea"], num["touch"], num["mineralsInSquare"], num["mineralsInHigh"], num["on"], num["in"], num["hang"], num["score"])
    return data
